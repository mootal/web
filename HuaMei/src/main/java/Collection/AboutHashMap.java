package Collection;

import java.util.HashMap;

/**
 * Created by Mono on 2016/5/1.
 */
public class AboutHashMap {

    public static void main(String[] args) throws Exception{
        HashMap demoMap = new HashMap();
        String demoStr = "12";
        Object defaultObj = "5";
        defaultObj.equals(1);
        System.out.println(defaultObj);

        byte bytes[] = new byte[2];
        byte a = 51;
        byte b = -128;
        bytes[0] = a;
        bytes[1] = b;
        System.out.println(b & 0xff);
        String byteStr = new String(bytes, 0, 2, "GBK");
        System.out.println(byteStr);
        System.out.println(byteStr.codePointAt(0));

        char[] charStr = new char[1];
        charStr[0] = 'a';
        System.out.println(new String(charStr));
        demoStr.getChars(0,1,charStr,0);
        System.out.println(new String(charStr));
    }

}
